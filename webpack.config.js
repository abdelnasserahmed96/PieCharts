const path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: [
		"react-hot-loader/patch",
		"webpack-dev-server/client?http://localhost:8080",
		"webpack/hot/only-dev-server",
		__dirname + "/src/index.js"
	],
	output: {
		path: __dirname + "/public",
		filename: "bundle.js",
		publicPath: "/"
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules|bower_component)/,
				loader: "babel-loader",
				options: { 
					presets: ["env", "react"],
					plugins: ['react-hot-loader/babel', 'transform-class-properties']
				}

			},
			{
				test: /\.css$/,
				use: ["style-loader", "css-loader"]
			}
		]
	},
	resolve: { extensions: ["*", ".js", ".jsx"] },
	devServer: {
		contentBase: "./public",
		historyApiFallback: true,
		inline: true,
		hotOnly: true
	},
	plugins: [new webpack.HotModuleReplacementPlugin()]
};


